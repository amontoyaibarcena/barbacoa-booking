import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Home from './views/Home/home'
import Admin from './views/Admin/admin'
import Booking from './views/Booking/booking'
import Management from './views/Management/management'
import * as serviceWorker from './serviceWorker'
import 'moment/locale/es'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { Switch } from 'react-router'

document.body.style.margin = '0px'

class App extends Component {
  render () {
    return (
      <Router>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/admin' component={Admin} />
          <Route path='/booking' component={Booking} />
          <Route path='/manage' component={Management} />
        </Switch>
      </Router>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
