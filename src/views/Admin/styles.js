import styled from 'styled-components'

const Container = styled.div`
  height: 100%;
  width: 100%;
  background-color: white;
`

const Content = styled.div`
  height: 100%;
  width: 100%;
  margin-top: 30px;
  display: flex;
  flex-direction: column;
  align-items: center;
  .main{
    height: 400px;
    width: 800px;
    display: flex;
    justify-content: space-around;
    .item1,
    .item2{
      height: 100%;
      width: 280px;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: space-around;
      font-size: 16px;
      background-color: #27231F;
      border-radius: 8px;
      cursor: pointer;
      img{
        width: 200px;
        height: 200px;
        border-radius: 50%;
      }
      .detail{
        display: flex;
        flex-direction: column;
        align-items: center;
        height: 150px;
        .title{
          color: white;
          margin-bottom: 10px;
        }
        .info{
          color: #FEDA15;
          text-align: center;
          vertical-align: top;
        }
      }
    }
    .item1{
      background-color: #DE0001;
    }
  }
`

export { Container, Content }