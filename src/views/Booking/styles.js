import styled from 'styled-components'

const Container = styled.div`
  height: 100%;
  width: 100%;
  background-color: white;
`

const Content = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  .list,
  .spin,
  .empty{
    width: 100%;
    margin-top: 70px;
  }
  .empty,
  .spin{
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  .spin{
    margin-top: 250px;
  }
  .empty{
    span{
      font-size: 14px;
      color: rgba(0, 0, 0, 0.45)
    }
  }
  .calendar{
    width: 100%;
    border: '1px solid #d9d9d9';
    borderRadius: 4;
    display: flex;
    align-items: top;
    justify-content: space-around;
    .time{
      display: flex;
      flex-direction: column;
      color: #C8651B;
      .time-item{
        display: flex;
        flex-direction: column;
        margin-bottom: 10px;
        .ant-time-picker-input{
          width: 150px;
        }
        .ant-time-picker-icon, 
        .ant-time-picker-clear{
          width: 0;
          right: 0;
        }
      }
    }
  }
`

export { Container, Content }