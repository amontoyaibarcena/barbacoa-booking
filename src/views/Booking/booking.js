import React, { Component } from 'react'
import Header from '../../components/Header'
import { Container, Content } from './styles'
import { List, Avatar, Button, Modal, Calendar, TimePicker, Spin, message } from 'antd'
import moment from 'moment'

const format = 'HH'

class Booking extends Component {
	constructor(props){
		super(props)
		this.state = {
			visible: false,
			listData: null,
			loading: true,
			saveBooking: false,
			editingIndex: 0,
			date: moment().format('DD-MM-YYYY'),
			startTime: 0,
			endTime: 1,
			disableStarHours: [],
			disableEndHours: [0]
		}
	}

	componentDidMount = () => {
		if (!sessionStorage.getItem('userId')) {
	  	this.props.history.push(`/`)
	  }
	  if (navigator.geolocation) {
    	navigator.geolocation.getCurrentPosition(position => {
    		let databody = {
					latitude: position.coords.latitude,
		      longitude: position.coords.longitude,
		    }
		    fetch(`${process.env.REACT_APP_HOST}/barbecues/location`, {
		      method: 'POST',
		      headers: {
		      	'Accept': 'application/json',
		      	'Content-Type':'application/json'
		      },
		      body: JSON.stringify(databody)
		    })
		    .then(res => {
		      res.json().then(result => {
		        this.setState({
		        	listData: result.data,
		        	loading: false
		        })
		      })
		      .catch(err => {
			      console.log(err);
			      this.setState({
		        	loading: false
		        })
			      res.status(500).json({
			        error: err
			      })
			    })
				})
    	})
	  }
	}

	showModal = index => {
    this.setState({
      visible: true,
      editingIndex: index
    })
  }

  handleOk = e => {
    this.setState({
      visible: false,
    })
  }

  handleCancel = e => {
    this.setState({
      visible: false,
    })
  }

	onChangeDate = value => {
		const { editingIndex, listData } = this.state

		this.setState({
			date: value.format('DD-MM-YYYY')
		})

		let databody = {
			id: listData[editingIndex]._id,
			date: value.format('DD-MM-YYYY')
    }
    fetch(`${process.env.REACT_APP_HOST}/barbecues/times`, {
      method: 'POST',
      headers: {
      	'Accept': 'application/json',
      	'Content-Type':'application/json'
      },
      body: JSON.stringify(databody)
    })
    .then(res => {
    	res.json().then(result => {
    		this.setState({
    			disableStarHours: result,
    			disableEndHours: result
    		})
      })
		})
	}

	changeStartTime = value => {
		const { disableEndHours } = this.state
		const start = parseInt(value.format('h'))+1
		let disabled = Array.from({length: start}, (x, index) => index)
		disabled = disableEndHours.concat(disabled)
		this.setState({
			disableEndHours: [...new Set(disabled)],
			startTime: parseInt(value.format('HH'))
		})
	}

	changeEndTime = value => {
		this.setState({
			endTime: parseInt(value.format('HH'))
		})
	}

	handleOk = e => {
		const { startTime, endTime, disableEndHours, editingIndex, date, listData } = this.state
		let range = []
		for(let i = startTime+1; i < endTime; i++){
			range.push(i)
		}
		let valid = range.filter(value => disableEndHours.includes(value))
		if(valid.length > 0){
			message.warning('El horario elegido contiene horas ocupadas')
		} else {
			if(endTime < startTime){
				message.error('El horario no es correcto')
			} else {
				this.setState({
		      saveBooking: true
		    })
				let databody = {
					id: listData[editingIndex]._id,
					userId: sessionStorage.getItem('userId'),
					date,
		      startTime,
		      endTime
		    }
		    fetch(`${process.env.REACT_APP_HOST}/barbecues/bookings`, {
		      method: 'PUT',
		      headers: {
		      	'Accept': 'application/json',
		      	'Content-Type':'application/json'
		      },
		      body: JSON.stringify(databody)
		    })
		    .then(res => {
		      this.setState({
			      visible: false,
			      saveBooking: false
			    })
				})
			}
		}
	}

	render() {
		const { listData, loading, disableStarHours, disableEndHours } = this.state

		return (
			<Container>
				<Header position={'fixed'} user={'Adrián'}/>
				<Content id='content'>
					{loading 
						? <div className='spin'><Spin size='large' tip="Cargando..."/></div> 
						: (!listData || listData.length === 0 ?
							<div className='empty'>
								<img alt='' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAVFBMVEX///+ZmZmWlpaTk5P5+fmioqLNzc25ubmxsbGoqKj4+Piampry8vL8/Pyfn5/u7u7Z2dnExMTk5OS+vr7Jycnd3d3o6Oi0tLTa2trT09Onp6eMjIzx/748AAAMgUlEQVR4nO1di5arKgwt4AMV8a3V8///eRN81trWVtROL3utc2fuTAfdkoQkkHi5GBgYGBgYGBgYGBgYGBgYGBgYGBgYGBgYGBj8f+H7/tm3sC8sGfCz72Ff+Iw5J15eOG7l1rtKUcFYtOf4T2G5jFFKGU12vEhF7R1Hfw5fUkIZkCRsP00BIa13G/wVbEpJLawoAYqVlhGFsOY/cqkMtYz9ARxG7VYDo4CwYvN4YRKAQHi3A6Vn2hmYwt4EFIxunsRIMopqzfjEcIU2tU+bwojSUfsCQrcOByqdpWlCKJVi+KlLWbpx4M+RMjrKD9zJtiUjDCgt1XcJWK6eosPYnmb6BfQyhAF6BYxpTytm1Ns06jaAlI66Jwnbpi7TwZpO/AsQ2FNdUjlaGpjPbSui1csoIqMNfgHzFYiHf3EEYLVotK0WNwwZMqyHxeg8wHIhY+ELJ6CbFwt7IgQuuEgCHEJ+t/wfDRGAM0MCyghrti5aOWN5/71N3YSe6ayNsCp0vMHzzjYPBasFy9o5Kxl6u/xcFRwQJZxXjo6bKQN4WlWeJxy8h7nv9hvwUTLRbYNIpXz98W+HSOPcbWbCHaZ1kthk48JzLkI/KpyMy3aylmxJTU90tTcAmMH8VJ5UzCgBwH/u19CIf8Ei+CaUOHp2MKGm1M12PTLPv4gMflf9lRm0oqIGcaR0YKaoUSJ5Eqc4TZyym78oYP4Y+XobGvoCxbEhnVlsmSE12VRAbbKuMDJmmEQMsQVlMl8Y8ltgiRLEkU/FsRPIwKuSuhRz7RJTZ53jA/Hi7xTQzjqC7z0y63UNqKX+g9supjnI+l9Vf4kPMyJU4miziTj2AhnYVadrT5AsLhbfAL8TRzpl1pkRCQIZl9EqYfOohvScZpQojo2kU3HsBJI27pKuPUEIYv1tcunfMut0TSpde3+0iJLg25b2K5vq2p3xfxMxIx/ElKEX2PvNvAvzB9xaXbvPvr+JhH7iYMPM76i9OH1V6WvKLHD6Sdi8K8MIhFRf/tmS5JPFQuzJ0AGGWzP3I+B5vfu4QiFECgxr+LqLjeKUbN99GQCGhr65s6s8O1AV/BLs4N/5AYx91TZc8vZiEZIJ9lhKU1RDfU/OgzDqzT+x1W6zsuds4ybJIuCha8yfhB+MJuI4doBgAl/3yFR59CPj9wBgaD7KsaIt3Wk/UeByr0/46w+f147rIdq+bVt5oQ+RZOwkrgdhyWIaagV2ZAguG/0kxWCJqLwCLd4mo9QmwAZz6MMgO2WLYa2g64e2RFnUOYRatiRtsHUTbqk5pO71A4NolTsFJOiyvbLumBMFKWyTouwu1GqzbiSQHoSSvRefpN+SmnHosu0LLSHKa51k3JP0VgpHXoxI2+NZXqdl5A/jtR9glDvrcgI7w7u10n5UpiCFlddIsshLTaJsuJs4cRqJu2wUMpTDAm678dnhfohCdWml0PVkQLpE7/10wYTZ3HXqohThk6kBhswScUV7ltROzjs3c8HMX+fxLmoXowGIYZU41zRaaQcUQ/wmqnnQJrRwU/Q8gc0mrMbpClrtKkC73r2xgeEFzWPOiWKpBPZ6isDavV3ACbO9Vrv8t3mNmDJE+GnS9AJLvOMtLEZOAK8uIk0PeM4QYRWZbFdKtLD1seeCuzkkPP8kcbiAJYYIcXVbnw4XywMFNrQHDUQZKrZf+BFDRFRXslNLCgJ7yJkaZKiebM+yyYptc/mMIV4wcrjygOBi2w+yrECIpwcKF7Vk8EWYzOLPWb5gqFDmHnrDhzF04asosuaGpe3W4iOzt4YhXpDpzH49Qc9QXbRIPMaG3TRYPKo3zR546GW2juGFHXT0ZMpQ/X+7eo3ec7DCuIcYUWHkEbQ6/dUM1c/S3JNTiYUgoby76dBXoYfL28hj6vOtZXjIIeElhgirdLhkEyMb8Bwj1BAC+wID+2YIqWaBx9o5DF6HpVrwiCHCguWL3rCEwD5YCj3aiArj37hMVuqh/QUMFYBlMCyYZMYLA3sbXHRwZQdSK23pRepm6JfXpTzIS4aIKO4crn7CAtnghBVLe41rGWLkvebG1yLHdFhwn1JbxRAh4gw1p4vsn1BYy5DrZcgZPv2FOrTVDC/41NfY93MYJmiaM5SLuaN0HkOXatwOshhh8QUrVgid6eJ5DNf6PqtQ9OfMqrujPD/CsGZd2t65O451HsOcatwMvbKORUaVtE5wHkP8nLZ0BkQqatOjvN9G+xGGoH+EJMXSXu95DGOmk2FbmQ3/gvm1z2WoMRfuVwwrcfmd3/YzDEEHndxZEIrzGKa6GT7AuQz1HeJ5jHMZxq8/thnnMYx+fg6R4RHn3c9jiAnTI5pG7MpQXGc7BP513PMOf4BhhhvjU1WrcbO5rzVBhkdUCu3IMFHH4yengIr2B93EhQ/OgOjGfgytdu91MjpvM3Zdhi0M/jpDv6tuG/+k26rsjtf+fYahbBmOLNw2H9kX7h1UFbyjHtZM5VfHgDRSKdd/fR6l+fMML7GktJl6+6UHMzgkin6A4SW8O98hJoQ5bfuc7IzzfBo0rUd0azMMp9DM0P15hhklRzSkO4ZhkbhVNq93/h2GftbuGjNm38T0+a8wvAbjBjJrJkfmgeER3bB2Z5iz/mxne2pv3BnKjykX3puhKm2kLkTCYZnLm8QFeHU/wBDrcWgyaFs8Y/huneJjiIeHrPdlaElC5bRaRXgjww8qMR8ipOwRi53nMGfe7cphjXxTjQzF4y6MOzP0nxzsShnRVuh0HsNnKHUxFGmKTQ7KdPGU/IkMhS6GY5n0EpMTGX5Q1b6Mf+0xO2ziu/DbM+eQatqaqbMM4hSZZdmSSJzJkOjbfPK/0tIIjV0AvtOW+oG+jQv/38PW8ScyRIdH29ZMvLSFr/ArDB/jRIZ46fuO3kJ3yHgiQ0wJzzcuajxarXdn+LsYZqr9vt56qDMZwni3l04ZraKoolo7D5zJkM8baWdMJVBtrZP4HkPSFKVvPU+QbWDI2y3wROtrUdaf1S+SoHXgie1VWVuLv/TB9QzduUy4TDkm3vFzGNWuTdhN+U/fT8FNnKKMpozeYnjrahWMJb6fMK09Tl4xtNLca9t5qCplMqsJ6kqdKJVtcbu/ieGl65KutS7xCUNLxJndVupR1ZAviSNL1XVVnq1aStxzhY/JlZVdmDC9c5ex7zfRu7v/gKGfDqVreONNVs9KinwRYfuMSVeQSU3UOoYpbq/NrZZINzRsXMQCQ1FkdjBMHeHO0zL20GrLEL3g39DQdQXDMpH4/A7o733LMCxr3tbjUVU+mr9V143tQ/LMfs0wyoPWbrEDtmYGhiFWAXfssAUyKN1nV39laaLc7srgmFxMO2gGMswuZV3ZtCuSZ407V7q38JShcJqutzljVXHMWzxAqIKhhDKo6s1P9TFD32n6KkbW1Ee9pKQM2gW8s4I2r9S6Jl75Zo/xgKFf95XwIJ3Jcb0/ClQ73ht92kLZRNlw5Zu93aFmiaEf82H2SHZkxyE8mNV6ENhSPnayrqd8z7XvMoRNodZ2GbpjGF6r3uWjjF8PfYVOhgTvPAi1vlXN2DFqMrPEbr2zJw0lZgyLqq8Gp9R2jm0RHfJFguPvRZnWqvJ+6qH13lmgvO4aI4zbWZkwtFJ3nD2ZHP3+FWFjb4A1L3ezhFrKu350k3cHdFJseyDF9bUrgB4Yppkc6FH3+P50JR6ReDMQC8NOWWd9InquJAD75Kl2r+CVjW0nvMOWhglitR/1uRc/7fVxG2Pgv0QOnRiYnZ/S4UudAdHx/kELlTW/U9ae3iF+2RJcdchF6wsWrW658WQ/mcf5ZXfwvbaNPtfR+OoO3fFu8MvOe3sAhn+9m00xiNB5K1wFJ/aBftki/LLOvJFm4EEMr2c6E0YYcU/tBDnCimLsCzXQtCtncwYBS1+d019teAtxTTyMnjrPjPJ8Qzs8PEBySIuytxEVOW9D4C43k32knIIc1PvpM4C/4lST7GHguW++gyVsjjkxug1W6riS9ckoRmT1PNU2Bd+vq7puhGntNsEotaCcxesGfBkjJ75J/AP4EawocpLz5s9XzvioFnN6gSsKpxPlfJiIK2HNOe9t91shioSTidCCcs49TvFeZ/evhMAVhbD+kBwLKnBre56hrfUNEucBVxS36V8oopQzi7GlYkXXbyX/AVhgauWEZtDgq3gOaRF4JMKydu1uRVER79cv9R/BKmP1flz6ha8D1AgfYxSt+QIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDg/PxH8ChjgXD9aNXAAAAAElFTkSuQmCC'/>
								<span><b>¡Lástima!</b> Nadie alquila barbacoas por aquí.</span>
							</div>
							:
							<div style={{ width: '55%' }}>
								<Modal
				          title="Reserva tu barbacoa"
				          visible={this.state.visible}
				          onOk={this.handleOk}
				          onCancel={this.handleCancel}
				          getContainer={() => {return document.getElementById('content')}}
				          okText='Reservar'
				          okButtonProps={{ loading: this.state.saveBooking }}
				          cancelText='Cancelar'
				        >
				          <div className='calendar'>
								    <Calendar 
								    	fullscreen={false} 
								    	onChange={this.onChangeDate}
								    	disabledDate={current => current < moment().startOf('day')}
								    />
								    <div className='time'>
									    <div className='time-item'>
									    	<span>Inicio</span>
									    	<TimePicker 
									    		placeholder='Selecciona hora' 
									    		onChange={this.changeStartTime} 
									    		defaultValue={moment('00:00', format)} 
									    		format={format}
									    		disabledHours={() => disableStarHours}
									    	/>
									    </div>
									    <div className='time-item'>
									    	<span>Fin</span>
									    	<TimePicker 
									    		placeholder='Selecciona hora' 
									    		onChange={this.changeEndTime} 
									    		defaultValue={moment('1:00', format)} 
									    		format={format}
									    		disabledHours={() => disableEndHours}
									    	/>
									    </div>
								    </div>
								  </div>
				        </Modal>

								<div className='list'>
									<List
								    itemLayout='vertical'
								    size='large'
								    dataSource={listData}
								    bordered={true}
								    renderItem={(item, index) => (
								      <List.Item
								        key={item._id}
								        actions={[<Button type='primary' onClick={() => this.showModal(index)}>Reservar</Button>]}
								        extra={
								          <img
								            width={272}
								            alt="logo"
								            src={item.path}
								          />
								        }
								      >
								        <List.Item.Meta
								          avatar={<Avatar src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAkFBMVEX/tAD/////sAD/sgD/tQD/rwD/9+X/47X///r/z3D/vzv/twD/z3P/6L7/+u7/2JD/3Jz/8tf/uyL/4Kj/79D//fT/7Mb/57v/ymT/v0T/9eL/xlf/9N3/+uz/5rb/uBP/1H//1Yf/467/vTL/yl7/xEz/2pb/uzT/vCr/7dL/8M3/3Jv/xVn/0nr/wEn/yGdt6iFUAAAPFUlEQVR4nOWd6WLiOAyAEyuhIVDuQEuADDDtsu0yff+329gKkPuSDOlUv3bYkvjDtiTLsmyYusXxpqv5fDz03dNsFliGFcxmJ9cfjufz1dRztL/f0Pjs7Xq3/3IHti2EACUGCv4j/NS2B+7XfrfeamyFLsLpU/81QDKjTJA0eO0/TTW1RAfhdOJbUMmW5gTLn+ig5CYc7c4DWzSAi2MKe3DejZhbxErYO/jQji5GCf6hx9koPkLv4BuCQnelFIZ/8NjaxUW46AsWvAuk6C+YWsZCOPo82nx4EaR9/GSZkgyEm7PF2H0xRmGdNx0gXITKRQMeSqh2yIOVSPjhsg/PpIDtfjyQcOFr5kNGWj8SCKe+lumXwyh8grPTmtAb3okPGYetDWRLQmeiUb/kiYBJy4VWO8Ll8b58ivG4vBuh07/jAL0JiH6bbmxBuLrzAL2JgNUdCEcvd7AQRQL2S2NPrinhMnhUB6KIoOlsbEi4f8gMjAuIvUbC9QNUaFbEca2L8Nl4dAeigPGsh3D88BF6ERBjDYSO34URehHh1zaNdQl7sy4BhoizuuGqmoSLoCsj9CIQ1FxT1SN8JoUI9QhAPX1Ti3D+QDemWMCecxFO7EfDFIg94SEcd0vHxKWO1agm/NVdwBDxF53wV1eHKIpdiVhF2OEhilI5UCsIJ10HDBEr1E054bzbQxSlwmiUEj5/B8AQsdT0lxEuOujJ5AlAmQNXQtjrnC9aJBCUuOHFhM7suwCGiLPixVQxYafWg1Ui/OaEnTeESSk2i0WEq+8FGCIWBYsLCNffRI3eBKAgAldAePpugCHiqQlhp9cTRVKwzsglXH5HwBAxN+CfRzj6NqY+KRDkbdvkEb58zy4MO/GlHuHqe/jbeWLnmIwsoaPTUGg2QgBZ7y1L2Nc4RsXxTS+j6FcT6tSjcutP85Isq0/ThM5RYwOEfMOzVj0Gx/Q4TROyBmbS3aUIs+4Ea69mwjYpQo/1ZYGbhBHv8h0jK/kSMXNZf1avlHDI+C4YOuZH8nkYbhgmCOXk5DRQYlhGOOX8MYWTmXW4Fk8oM7Dki+eMLxbTEkKfdZCqOd9PPNKWqq6XIBxwvxn8YsIFq5rDZfcmOSQ/w4+cxJgUKtz5L2cnLgoJaT9k5pAMzjo3/qk4hJ9sU/pHBsrSVoqiX5OdGCdckOY7uL9TaxL4Tz71HPsQYJQdKqD85ZfEd+HNJzDaiwJCl9KFEKqwUbIjYCYV93uMB43Ve4rwH/lhQsHCLtRH7VsDbj7hlNSFQh4cWCdf9CYfGzPwUcwvPSD9dB9iCwn61Z7mEtJmIU7vRAwSdcjb9bEQKO16SLUclzx/4oTHvF+igcRn4o1wQ9PX+NBp7CHit/xkG/tAnQ9JbxaghV4nVe48ZzQ3as0mh5AafRLh3DG92Esw0n5rpjjLf2eUppvzdjDkDCb46LGo1JVwZLV+HIpKjHCC20tU/LJ3c0IxoPk7pWYsdUJ2kx65WxqhYY0yhJ/ELkTFeRvq+Cs6t8Ar/JEf7FLqDJ1xL+2Mn4mqBl2LJCF1XQjTZKNw0sWCWpj5+pp+TS9HycGrmdRRLZpzTBPSrP3lN7t1GbjK7449VWmPrZU3C9Odpbyc9LqkoVyt/oWwT9SkFj7l2qiso6LiC+v095Qi/S+FrYItHm3/EvpJQo86C1VLY4kN+EHcS8rtQzUN0yy4jfRG1e1egjBthRs/TimMWJY0fKVnEs7DlGeYOzntNcO0QSf/RkhdnuE0jDUKHYC4i4M+XCp/BZRXcE7+vspUpJVuY7n4NUjYIz4t6ov4dgAapLj/AjlqEx251EaJLc9Upg1IC+nFCKmDNFLO8RgTDpK4PkQL6Q2S/pnKhdkl3o9DnrwLHQ1Tg2WQXpRzbAkEMzPTicqp2Sc7EU1f0mdTgQ2PugMWDVNFOKIHSXAHNq4yVIDdSaz6ZmntY0Sxjl6yv9SY75EbpZbbSLijDgjZqreel3SfxS+vl8SBP2tvn36XPfY26bW3GHtr0nocn7K7Ep7pfajqIKRdk0yZhezf5P5Z/mfNW3S+Eg44CLsnOJ8lIS180WFRwQxJSNuNkbVzdAnQxqoKfBlUWyFO83VPl6znJ8qvr+yFJKR4DxiM0Si/CYhqxWMko0eNn+FWNpEqFKshl+Uh4RMlWNDkrGM7oURrxJMipCx+VRzJe9Ilcom3JYTI5DLYyAmdNHjCSbZhLEvO6RBbenQeIYtQer2GuSV4uBDIPuTc3kwIhgWI7TPMNUkfy4ga77ZjTNROASkWL9YhIcntFv+aNGVcKipCSdo7DZ1vw8w4+42ecCDqglJReoy0Og/XYYb5RRoFe6nQNWUU45qT1APwFRLS9kXVNi8x2Fr4cBXy/I/UPtc0HNLSCQJpLig+Q4lIe00MZsDAMTza0smWynSjZ/nF8WzbM4iLQ9zL1LKExgUs0djaU4MYtMP9CZI2KJIoBkv78cTKoDokKt9QS5RArdAdavPmdMKdJnuBtoIaBgwJx9S4q1oCE5IKigQj37+pzRsbQ/KvL+0FeeMjR+S2g1f9Z+UCQ4Mc0Edtym70cQOSnLIMvkEOLWNmNWtmqhSVB0DPOgfXoOsIjGRQJ0xK0ArRT0HCyaCf9sU9nh6vwbDV5h8lGwMFZsaMoTVLdquP1n7J8KvNjKD6j6oELRd5vy/+ROXQmxynyQODY/VqH3gmzVUwG+PAMfB5VudRWiXbiSk8veQwDQoWxijtkJ5coAQs9YOdWX4wi2MeGpcDVUxBN0zGZTpiFnDoUuM6TlmqvWA1Fq4xOmOwh0qiA6oMJiOq3sl0VDe0h1zrnujMGLlh0U/FdYYu9GnoKQ+XZ+HZRmLBkGg/knAUIdUql762uD7LWtMRo/MKayatrNYW9PXh9WHoiFDMYnSMl9E9CteH1DV+/GlHTB9vXfglKsEyYjypG67xObfGwMVePLRKoYAo1c5jUw0GSyQqIZde3LRIoRCnDXsPKkLeUjsQRIdVzg27EdDxC38c3qIcYkWNeacFjOhA/LJRN4rT5WvM9abtKXXfIiNwPcsxycnSyxdxPWL+yV1v2vaIe0+5D7001zvXueIKhHGO0upHb9yxc7n3RNs/zBNxqxKzHQcVjCCC8eVuwO2APa4s9w9pe8B5D02ch/cOvl0ICcJO3D9GDzxl3vBF3cfPETzwFLzfOnLiqosQk6+WybTuJHm1I3/JDLWPz5ECHRPcx1jZYra71eBwluO3fyy4pgKB9c/beBn7/7uBOhjGXshQ5WKQ8mlynrmMxhvYp3miRMV2s1jNP8ef89Vik+g7b36y6efx8luzJuZEZQUPXmCMX2qR6ovTNqiNVGK9w9gS1QKVE0XJa8sK+pYX7RWOy9Ok7Kam0eQU5ayj181crRHz2li3jfCUXvyoRAjp7pd5l8Vul3s3npKf/iZHc/rk/NK04LZmSj2HRsF6fdk/LaZbnI/TxdP+5dVKmhHsfd5duii/lDMtTZW/yZtNyjrgMRpX/XemKER8BrM1Z0rP804KHt4qjCJdCAv+70ULs8klz5uxNgwe9C9UzlBKiLt0HPtNiSfSz1vEn4jWvvBx5YQXb4ivE6/nLdiWiDjO/hQ2sYIwOkzMNxOvZ2a4krbwaELJ5kUFIa5K+JZzt3NPPGfXLslDJWlalYS8Vj92do3H+Y6sfcmzqghxW40tNyd2/pDhDKlx2bkoK5FSRRjl5nAFqWNnSHnshTq97ZQ9qZKQ1erHzwHTz3IbldZevbOKMMrN4bH6ibPcHEMfrX3pGraakNXqx8/jMwxTTGIq11nVhFENK461frKmAsMwxc33YmuvXlpNiPn5LLMmWReDWtsk0hEVqQo1CA1b6iuO7bVUbRPyusyusvZS6hDiNj7d6qfr01CLpUBQZe2vb60gBIt66BAlU2OIWCeq2tpLqUPIZPWzdaJotb6g2tqrP6tDiP471ern1Poi1WvDyVOZzl6LMLrciGj1c+q1kdLIVF2W6qt36hFiMaIPWp3Ks5klJJzVxLV99QqlHmFk9UmKIbduIsGvQb1VHVmuSYjuEcXq59e+bG8wImtf/fWahFgKi3LfVLwILUcNWixAWiNZvy5hTcVV/J6CGrRtJzcWR6qTq1+XELOrtlV/ViiqWFgeYcuZiNa+jiquS0i0+sW1oNvl+OIPXisQUpsQz+GsWyqGknrerTqxwaSpTUha65fVZG/lK2HB8VqKrz4hlnFrpxhK6+q3uBuhyZK1PmGk79t0YvndCC3ut4A/z05dQ9OAUFp959DGr6m436LFLg2I47xm+l8DQgM+3mdtUsCq7ihpdeKvdkWuJoT1n5r8VuU9M1rvCsoWpmWX6ruCtN73BO77+H2s837TOvc9ab2zS23mawSsd2fX33/v2g+4O+8H3H/4999h+QPuIf0Bd8n+gPuA//47nX/Avdw/4G51Urzy3hLd2tOQMHNtUXcFgl4xRgmh7ktR2QRgUUJRRpi4JqbDYpeW+i0l5KkgoFuwQkFLQt6rZfVIJjDTjLD7ZrHYENYkNH91e6Da+e52E8JurzMK1hPNCLs8UCuHaD1Cc9LVgWpXKJnahKHR6KLphwoz0YTQfO6gdwNQr6Z/PUJz0TkfFYIyV605odmbdUvfiFmJs92K0HQ6tV4UfvFyqS2htBpdGalQx0q0IDRXHdE3AEUxGSqhuSbda8Ml4lQQVWMglC7co7sRajhqFEJzGTy2G0WQG7pnJDRHLw90cMB+ydt84SWUCudR3SgaqZj2hKbTf8hsBNGvbQSJhOFsPN6/G8Wx6QykEJrO5M5DVcCkTQe2JzRNb3jHoQpiWFZdQw+haU79OzGC8KfVzdFAGK6p/DtYDrD9muskDYSm+eFqZgTb/ahuhkZC2Y8adY4AWv+xEJrm5mxpmZAgrHN1iaJ7EIae3OeRfbCCffxs7KHlCQthKIs+Z8IaCNEnD89IuAhV7bk6RQTr4BmJOnVE4SMMpXcI1Q4pEADh9/1D3SBTLWElDGW0Ow/sdpQhnT0471gmX0y4CaVMJ768q7gBJsi+s/wJwXUpFB2EUqZP/ddAFbyqOBOrymIFr/0nHXRSdBFK2a53+y93YNtIeoPFf8irVO2B+7XfrfPKuXGJTkIUx5uu5vPx0HdPs1lgGVYwm51cfziez1dTr+WSqIH8Dw44xlnTHzzpAAAAAElFTkSuQmCC' />}
								          title={item.model}
								          description={item.description}
								        />
								      </List.Item>
								    )}
								  />
							  </div>
						  </div>
						 )
				  }}
				</Content>
			</Container>
		)
	}
}

export default Booking
