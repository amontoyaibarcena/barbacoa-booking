import styled from 'styled-components'
import { Form } from 'antd'

const Container = styled.div`
  height: 100%;
  width: 100%;
  background-color: white;
`

const Content = styled.div`
  height: calc(100vh - 60px);
  width: 100%;
  padding: 20px;
  padding-top: 30px;
  display: flex;
  justify-content: center;
  background-image: url('http://revistaelconocedor.com/wp-content/uploads/2017/01/carne-1.jpg');
  background-size: cover;
  background-repeat: no-repeat;
  .welcome{
  	font-size: 16px;
  	color: white;
  	margin-right: 20px;
  	h1{
  		color: white;
  	}
  }
`

const CustomForm = styled(Form)`
  width: 300px;
  height: 200px;
  padding: 20px;
  border-radius: 6px;
  background-color: rgba(39, 35, 31, 0.7);
  .login-form-button{
    margin-right: 10px;
  }
`

export { Container, Content, CustomForm }