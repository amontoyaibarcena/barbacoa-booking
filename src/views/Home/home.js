import React, { Component } from 'react'
import Header from '../../components/Header'
import Login from '../../components/Login'
import Registration from '../../components/Registration'
import { Container, Content } from './styles'

class Home extends Component {
	constructor(props){
		super(props)
		this.state  = {
			user: null,
			view: 'login'
		}
	}

	componentDidMount = () => {
		if (sessionStorage.getItem('userId')) {
		  this.props.history.push(`/admin`)
	  } else {
	  	this.props.history.push(`/`)
	  }
	}

	changeView = view => {
		this.setState({
			view
		})
	}

	render() {
		const { user, view } = this.state

		return (
			<Container>
				<Header user={user} login/>
				<Content>
					{ 
						view === 'login'
						? <Login changeView={this.changeView}/>
						: <Registration changeView={this.changeView}/>
					}
				</Content>
			</Container>
		)
	}
}

export default Home
