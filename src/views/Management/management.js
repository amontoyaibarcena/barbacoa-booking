import React, { Component } from 'react'
import Header from '../../components/Header'
import Barbecue from '../../components/FormBarbecue'
import { Container, Content } from './styles'
import { Card, Icon, Avatar, Input, Modal, Table } from 'antd'

const { Meta } = Card
const { TextArea } = Input

class Management extends Component {
	constructor(props){
		super(props)
		this.state = {
	    indexEditing: null,
	    visible: false,
	    dataSource: []
		}
	}

	componentDidMount = () => {
		if (!sessionStorage.getItem('userId')) {
	  	this.props.history.push(`/`)
	  }
    fetch(`${process.env.REACT_APP_HOST}/barbecues/get/${sessionStorage.getItem('userId')}`, {
      method: 'GET',
      headers: {
      	'Accept': 'application/json',
      	'Content-Type':'application/json'
      },
    })
    .then(res => {
      res.json().then(result => {
      	this.setState({
      		data: result
      	})
      })
    })
	}

	updateData = newElement => {
		let { data } = this.state
		data.push(newElement)
		this.setState({
			data
		})
	}

	editCard = (index) => {
		this.setState({
			indexEditing: index
		})
	}

	onChangeModel = (e, index) => {
		const { data } = this.state
		data[index].model = e.target.value
		this.setState({
			data
		})
	}

	onChangeDescription = (e, index) => {
		const { data } = this.state
		data[index].description = e.target.value
		this.setState({
			data
		})
	}

	updateBarbecue = () => {
		const { indexEditing, data } = this.state
		let databody = {
			_id: data[indexEditing]._id,
      model: data[indexEditing].model,
      description: data[indexEditing].description
    }
    fetch(`${process.env.REACT_APP_HOST}/barbecues/update`, {
      method: 'PUT',
      headers: {
      	'Accept': 'application/json',
      	'Content-Type':'application/json'
      },
      body: JSON.stringify(databody)
    })
    .then(res => {
      res.json().then(result => {
        this.setState({
					indexEditing: null
				})
      })
      .catch(err => {
	      console.log(err);
	      res.status(500).json({
	        error: err
	      });
	    });
		})
	}

	deleteBarbecue = index => {
		const {  data } = this.state
		fetch(`${process.env.REACT_APP_HOST}/barbecues/delete/${data[index]._id}`, {
      method: 'DELETE',
      headers: {
      	'Accept': 'application/json',
      	'Content-Type':'application/json'
      }
    })
    .then(res => {
      res.json().then(result => {
        data.splice(index, 1)
        this.setState({
        	data
        })
      })
      .catch(err => {
	      console.log(err);
	      res.status(500).json({
	        error: err
	      });
	    });
		})
	}

	checkBookings = index => {
		const { data } = this.state
		this.setState({
			visible: true
		})
		fetch(`${process.env.REACT_APP_HOST}/barbecues/bookings/${data[index]._id}`, {
      method: 'GET',
      headers: {
      	'Accept': 'application/json',
      	'Content-Type':'application/json'
      }
    })
    .then(res => {
      res.json().then(result => {
      	let rows = []
       	result.bookings.forEach(booking => {
       		booking.detail.forEach(detail => {
       			rows.push({
       				key: detail._id,
       				name: detail.userId.firstName + ' ' + detail.userId.lastName,
       				date: booking.date,
       				hours: detail.range[0].toString() + '-' + detail.range[detail.range.length-1].toString()
       			})
       		})
       	})
       	this.setState({
       		dataSource: rows
       	})
      })
      .catch(err => {
	      console.log(err);
	      res.status(500).json({
	        error: err
	      });
	    });
		})
	}

	onCancel = () => {
		this.setState({
			visible: false
		})
	}

	render() {
		const { data, dataSource, indexEditing, visible } = this.state

		const columns = [
		  {
		    title: 'Usuario',
		    dataIndex: 'name',
		    key: 'name',
		  },
		  {
		    title: 'Fecha',
		    dataIndex: 'date',
		    key: 'date',
		    defaultSortOrder: 'ascend',
		    sorter: (a, b) => a.date > b.date,
		  },
		  {
		    title: 'Horas',
		    dataIndex: 'hours',
		    key: 'hours',
		  },
		]

		return (
			data ?
			<Container>
				<Header position={'fixed'} />
				<Content id='content'>
					<div>
						<Barbecue updateData={this.updateData}/>
					</div>

					<Modal
	          title="Bookings"
	          visible={visible}
	          onCancel={this.onCancel}
	          footer={null}
	          getContainer={() => {return document.getElementById('content')}}
	        >
	          <div className='bookings'>
	          	<Table dataSource={dataSource} columns={columns} pagination={false}/>;
					  </div>
	        </Modal>

					{data.map((item, index) => 
						<Card
					    className='card'
					    key={index}
					    cover={<img alt="" src={item.path}/>}
					    actions={[
					    	<Icon theme='filled' type="calendar" style={{ color: '#27231F' }} onClick={() => this.checkBookings(index)}/>,
					    	index !== indexEditing ? <Icon theme='filled' type="edit"  style={{ color: '#27231F' }} onClick={() => this.editCard(index)}/> : <Icon type="check"  style={{ color: '#27231F' }} onClick={this.updateBarbecue}/>, 
					    	<Icon theme='filled' type="delete" style={{ color: 'red' }} onClick={() => this.deleteBarbecue(index)}/>
					    ]}
					  >
					    <Meta
					      avatar={index !== indexEditing && <Avatar src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAkFBMVEX/tAD/////sAD/sgD/tQD/rwD/9+X/47X///r/z3D/vzv/twD/z3P/6L7/+u7/2JD/3Jz/8tf/uyL/4Kj/79D//fT/7Mb/57v/ymT/v0T/9eL/xlf/9N3/+uz/5rb/uBP/1H//1Yf/467/vTL/yl7/xEz/2pb/uzT/vCr/7dL/8M3/3Jv/xVn/0nr/wEn/yGdt6iFUAAAPFUlEQVR4nOWd6WLiOAyAEyuhIVDuQEuADDDtsu0yff+329gKkPuSDOlUv3bYkvjDtiTLsmyYusXxpqv5fDz03dNsFliGFcxmJ9cfjufz1dRztL/f0Pjs7Xq3/3IHti2EACUGCv4j/NS2B+7XfrfeamyFLsLpU/81QDKjTJA0eO0/TTW1RAfhdOJbUMmW5gTLn+ig5CYc7c4DWzSAi2MKe3DejZhbxErYO/jQji5GCf6hx9koPkLv4BuCQnelFIZ/8NjaxUW46AsWvAuk6C+YWsZCOPo82nx4EaR9/GSZkgyEm7PF2H0xRmGdNx0gXITKRQMeSqh2yIOVSPjhsg/PpIDtfjyQcOFr5kNGWj8SCKe+lumXwyh8grPTmtAb3okPGYetDWRLQmeiUb/kiYBJy4VWO8Ll8b58ivG4vBuh07/jAL0JiH6bbmxBuLrzAL2JgNUdCEcvd7AQRQL2S2NPrinhMnhUB6KIoOlsbEi4f8gMjAuIvUbC9QNUaFbEca2L8Nl4dAeigPGsh3D88BF6ERBjDYSO34URehHh1zaNdQl7sy4BhoizuuGqmoSLoCsj9CIQ1FxT1SN8JoUI9QhAPX1Ti3D+QDemWMCecxFO7EfDFIg94SEcd0vHxKWO1agm/NVdwBDxF53wV1eHKIpdiVhF2OEhilI5UCsIJ10HDBEr1E054bzbQxSlwmiUEj5/B8AQsdT0lxEuOujJ5AlAmQNXQtjrnC9aJBCUuOHFhM7suwCGiLPixVQxYafWg1Ui/OaEnTeESSk2i0WEq+8FGCIWBYsLCNffRI3eBKAgAldAePpugCHiqQlhp9cTRVKwzsglXH5HwBAxN+CfRzj6NqY+KRDkbdvkEb58zy4MO/GlHuHqe/jbeWLnmIwsoaPTUGg2QgBZ7y1L2Nc4RsXxTS+j6FcT6tSjcutP85Isq0/ThM5RYwOEfMOzVj0Gx/Q4TROyBmbS3aUIs+4Ea69mwjYpQo/1ZYGbhBHv8h0jK/kSMXNZf1avlHDI+C4YOuZH8nkYbhgmCOXk5DRQYlhGOOX8MYWTmXW4Fk8oM7Dki+eMLxbTEkKfdZCqOd9PPNKWqq6XIBxwvxn8YsIFq5rDZfcmOSQ/w4+cxJgUKtz5L2cnLgoJaT9k5pAMzjo3/qk4hJ9sU/pHBsrSVoqiX5OdGCdckOY7uL9TaxL4Tz71HPsQYJQdKqD85ZfEd+HNJzDaiwJCl9KFEKqwUbIjYCYV93uMB43Ve4rwH/lhQsHCLtRH7VsDbj7hlNSFQh4cWCdf9CYfGzPwUcwvPSD9dB9iCwn61Z7mEtJmIU7vRAwSdcjb9bEQKO16SLUclzx/4oTHvF+igcRn4o1wQ9PX+NBp7CHit/xkG/tAnQ9JbxaghV4nVe48ZzQ3as0mh5AafRLh3DG92Esw0n5rpjjLf2eUppvzdjDkDCb46LGo1JVwZLV+HIpKjHCC20tU/LJ3c0IxoPk7pWYsdUJ2kx65WxqhYY0yhJ/ELkTFeRvq+Cs6t8Ar/JEf7FLqDJ1xL+2Mn4mqBl2LJCF1XQjTZKNw0sWCWpj5+pp+TS9HycGrmdRRLZpzTBPSrP3lN7t1GbjK7449VWmPrZU3C9Odpbyc9LqkoVyt/oWwT9SkFj7l2qiso6LiC+v095Qi/S+FrYItHm3/EvpJQo86C1VLY4kN+EHcS8rtQzUN0yy4jfRG1e1egjBthRs/TimMWJY0fKVnEs7DlGeYOzntNcO0QSf/RkhdnuE0jDUKHYC4i4M+XCp/BZRXcE7+vspUpJVuY7n4NUjYIz4t6ov4dgAapLj/AjlqEx251EaJLc9Upg1IC+nFCKmDNFLO8RgTDpK4PkQL6Q2S/pnKhdkl3o9DnrwLHQ1Tg2WQXpRzbAkEMzPTicqp2Sc7EU1f0mdTgQ2PugMWDVNFOKIHSXAHNq4yVIDdSaz6ZmntY0Sxjl6yv9SY75EbpZbbSLijDgjZqreel3SfxS+vl8SBP2tvn36XPfY26bW3GHtr0nocn7K7Ep7pfajqIKRdk0yZhezf5P5Z/mfNW3S+Eg44CLsnOJ8lIS180WFRwQxJSNuNkbVzdAnQxqoKfBlUWyFO83VPl6znJ8qvr+yFJKR4DxiM0Si/CYhqxWMko0eNn+FWNpEqFKshl+Uh4RMlWNDkrGM7oURrxJMipCx+VRzJe9Ilcom3JYTI5DLYyAmdNHjCSbZhLEvO6RBbenQeIYtQer2GuSV4uBDIPuTc3kwIhgWI7TPMNUkfy4ga77ZjTNROASkWL9YhIcntFv+aNGVcKipCSdo7DZ1vw8w4+42ecCDqglJReoy0Og/XYYb5RRoFe6nQNWUU45qT1APwFRLS9kXVNi8x2Fr4cBXy/I/UPtc0HNLSCQJpLig+Q4lIe00MZsDAMTza0smWynSjZ/nF8WzbM4iLQ9zL1LKExgUs0djaU4MYtMP9CZI2KJIoBkv78cTKoDokKt9QS5RArdAdavPmdMKdJnuBtoIaBgwJx9S4q1oCE5IKigQj37+pzRsbQ/KvL+0FeeMjR+S2g1f9Z+UCQ4Mc0Edtym70cQOSnLIMvkEOLWNmNWtmqhSVB0DPOgfXoOsIjGRQJ0xK0ArRT0HCyaCf9sU9nh6vwbDV5h8lGwMFZsaMoTVLdquP1n7J8KvNjKD6j6oELRd5vy/+ROXQmxynyQODY/VqH3gmzVUwG+PAMfB5VudRWiXbiSk8veQwDQoWxijtkJ5coAQs9YOdWX4wi2MeGpcDVUxBN0zGZTpiFnDoUuM6TlmqvWA1Fq4xOmOwh0qiA6oMJiOq3sl0VDe0h1zrnujMGLlh0U/FdYYu9GnoKQ+XZ+HZRmLBkGg/knAUIdUql762uD7LWtMRo/MKayatrNYW9PXh9WHoiFDMYnSMl9E9CteH1DV+/GlHTB9vXfglKsEyYjypG67xObfGwMVePLRKoYAo1c5jUw0GSyQqIZde3LRIoRCnDXsPKkLeUjsQRIdVzg27EdDxC38c3qIcYkWNeacFjOhA/LJRN4rT5WvM9abtKXXfIiNwPcsxycnSyxdxPWL+yV1v2vaIe0+5D7001zvXueIKhHGO0upHb9yxc7n3RNs/zBNxqxKzHQcVjCCC8eVuwO2APa4s9w9pe8B5D02ch/cOvl0ICcJO3D9GDzxl3vBF3cfPETzwFLzfOnLiqosQk6+WybTuJHm1I3/JDLWPz5ECHRPcx1jZYra71eBwluO3fyy4pgKB9c/beBn7/7uBOhjGXshQ5WKQ8mlynrmMxhvYp3miRMV2s1jNP8ef89Vik+g7b36y6efx8luzJuZEZQUPXmCMX2qR6ovTNqiNVGK9w9gS1QKVE0XJa8sK+pYX7RWOy9Ok7Kam0eQU5ayj181crRHz2li3jfCUXvyoRAjp7pd5l8Vul3s3npKf/iZHc/rk/NK04LZmSj2HRsF6fdk/LaZbnI/TxdP+5dVKmhHsfd5duii/lDMtTZW/yZtNyjrgMRpX/XemKER8BrM1Z0rP804KHt4qjCJdCAv+70ULs8klz5uxNgwe9C9UzlBKiLt0HPtNiSfSz1vEn4jWvvBx5YQXb4ivE6/nLdiWiDjO/hQ2sYIwOkzMNxOvZ2a4krbwaELJ5kUFIa5K+JZzt3NPPGfXLslDJWlalYS8Vj92do3H+Y6sfcmzqghxW40tNyd2/pDhDKlx2bkoK5FSRRjl5nAFqWNnSHnshTq97ZQ9qZKQ1erHzwHTz3IbldZevbOKMMrN4bH6ibPcHEMfrX3pGraakNXqx8/jMwxTTGIq11nVhFENK461frKmAsMwxc33YmuvXlpNiPn5LLMmWReDWtsk0hEVqQo1CA1b6iuO7bVUbRPyusyusvZS6hDiNj7d6qfr01CLpUBQZe2vb60gBIt66BAlU2OIWCeq2tpLqUPIZPWzdaJotb6g2tqrP6tDiP471ern1Poi1WvDyVOZzl6LMLrciGj1c+q1kdLIVF2W6qt36hFiMaIPWp3Ks5klJJzVxLV99QqlHmFk9UmKIbduIsGvQb1VHVmuSYjuEcXq59e+bG8wImtf/fWahFgKi3LfVLwILUcNWixAWiNZvy5hTcVV/J6CGrRtJzcWR6qTq1+XELOrtlV/ViiqWFgeYcuZiNa+jiquS0i0+sW1oNvl+OIPXisQUpsQz+GsWyqGknrerTqxwaSpTUha65fVZG/lK2HB8VqKrz4hlnFrpxhK6+q3uBuhyZK1PmGk79t0YvndCC3ut4A/z05dQ9OAUFp959DGr6m436LFLg2I47xm+l8DQgM+3mdtUsCq7ihpdeKvdkWuJoT1n5r8VuU9M1rvCsoWpmWX6ruCtN73BO77+H2s837TOvc9ab2zS23mawSsd2fX33/v2g+4O+8H3H/4999h+QPuIf0Bd8n+gPuA//47nX/Avdw/4G51Urzy3hLd2tOQMHNtUXcFgl4xRgmh7ktR2QRgUUJRRpi4JqbDYpeW+i0l5KkgoFuwQkFLQt6rZfVIJjDTjLD7ZrHYENYkNH91e6Da+e52E8JurzMK1hPNCLs8UCuHaD1Cc9LVgWpXKJnahKHR6KLphwoz0YTQfO6gdwNQr6Z/PUJz0TkfFYIyV605odmbdUvfiFmJs92K0HQ6tV4UfvFyqS2htBpdGalQx0q0IDRXHdE3AEUxGSqhuSbda8Ml4lQQVWMglC7co7sRajhqFEJzGTy2G0WQG7pnJDRHLw90cMB+ydt84SWUCudR3SgaqZj2hKbTf8hsBNGvbQSJhOFsPN6/G8Wx6QykEJrO5M5DVcCkTQe2JzRNb3jHoQpiWFZdQw+haU79OzGC8KfVzdFAGK6p/DtYDrD9muskDYSm+eFqZgTb/ahuhkZC2Y8adY4AWv+xEJrm5mxpmZAgrHN1iaJ7EIae3OeRfbCCffxs7KHlCQthKIs+Z8IaCNEnD89IuAhV7bk6RQTr4BmJOnVE4SMMpXcI1Q4pEADh9/1D3SBTLWElDGW0Ow/sdpQhnT0471gmX0y4CaVMJ768q7gBJsi+s/wJwXUpFB2EUqZP/ddAFbyqOBOrymIFr/0nHXRSdBFK2a53+y93YNtIeoPFf8irVO2B+7XfrfPKuXGJTkIUx5uu5vPx0HdPs1lgGVYwm51cfziez1dTr+WSqIH8Dw44xlnTHzzpAAAAAElFTkSuQmCC' />}
					      title={index === indexEditing ? <Input value={item.model} onChange={(e) => this.onChangeModel(e, index)}/> : item.model}
					      description={index === indexEditing ? <TextArea onChange={(e) => this.onChangeDescription(e, index)} autosize={{minRows: 3.2, maxRows: 3.2}} value={item.description}/> : item.description}
					    />
					  </Card>
					)}
				</Content>				  
			</Container>
			: null
		)
	}
}

export default Management
