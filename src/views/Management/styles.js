import styled from 'styled-components'

const Container = styled.div`
  height: 100%;
  width: 100%;
  background-color: white;
`

const Content = styled.div`
  height: 100%;
  width: 100%;
  padding-top: 80px;
  display: flex;
  overflow: auto;
  flex-wrap: wrap;
  justify-content: center;
  .add{
    height: 420px;
    width: 300px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-around;
    background-color: rgba(39, 35, 31, 0.7);
    padding: 15px;
    margin-left: 15px;
    border-radius: 4px;
    .ant-upload-list-picture-card .ant-upload-list-item,
    .ant-upload.ant-upload-select-picture-card{
      width: 200px;
      height: 200px;
    }
    .anticon-eye-o{
      display: none;
    }
  }
  .card{
    width: 300px;
    min-width: 300px;
    margin-bottom: 10px;
    margin-left: 15px;
    border-radius: 4px;
  }
  .ant-card-meta {
    height: 130px;
  }
  .ant-card-cover img {
    height: 230px;
  }
  .ant-card-meta-description {
    overflow: auto;
    max-height: 100px;
    word-break: break-word;
  }
  .ant-card-actions{
    background-color: rgba(254, 218, 21, 0.6)
  }
  .bookings{
    height: 350px;
    overflow-y: auto;
  }
`

export { Container, Content }