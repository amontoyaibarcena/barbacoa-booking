import styled from 'styled-components'

const Container = styled.div`
  height: 60px;
  width: 100%;
  background-color: rgba(39, 35, 31, 1);
  position: ${props => (props.position ? props.position : 'relative')}
  display: flex;
  justify-content: space-between;
  align-items: center;
  z-index: 1;
  box-shadow: 0 2px 5px 0 rgba(0,0,0,0.1);
  .logo{
    color: #C8651B;
    font-weight: bold;
    font-size: 22px;
    font-style: italic;
    padding-left: 20px;
    cursor: pointer;
  }
  .user{
    color: white;
    font-size: 15px;
    padding-right: 20px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    i{
      font-size: 18px;
      margin-left: 10px;
      cursor: pointer;
    }
  }
`

export { Container }