import React, { Component } from 'react'
import { Icon } from 'antd'
import { Container } from './styles'
import { withRouter } from 'react-router-dom'

class Header extends Component {
	constructor(props){
		super(props)
		this.state = {
			user: null
		}
	}

	componentDidMount = () => {
		if (sessionStorage.getItem('userId') && !this.state.user) {
			fetch(`${process.env.REACT_APP_HOST}/users/get/${sessionStorage.getItem('userId')}`)
		    .then(res => {
		      res.json().then(result => {
		        this.setState({
		          user: result.firstName + ' ' + result.lastName
		        })
		      })
		    })
	  }
	}

	logout = () => {
		sessionStorage.clear()
    this.props.history.push('/')
	}

	render() {
		const { user } = this.state
		return (
			<Container position={this.props.position}>
				<div className='logo' onClick={() => this.props.history.push('/') }>BarbacoasFinder.com</div>
				{
					!this.props.login && 
					<div className='user'>
						<div>Bienvenido, {user}</div>
						<Icon onClick={this.logout} type='logout'/>
					</div>
				}
			</Container>
		)
	}
}

export default withRouter(Header)