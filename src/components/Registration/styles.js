import styled from 'styled-components'
import { Form } from 'antd'

const Container = styled.div`
  height: 100%;
  width: 100%
`
const Content = styled.div`
  height: 100%;
  width: 100%;
  padding: 20px;
  display: flex;
  justify-content: center;
`
const CustomForm = styled(Form)`
  width: 300px;
  padding: 20px !important;
  border-radius: 6px;
  background-color: rgba(39, 35, 31, 0.7);
  .title{
    color: white;
    font-size: 18px;
    margin-bottom: 10px;
  }
  .login-form-button{
    margin-right: 10px;
  }
`

export { Container, Content, CustomForm }