import React, { Component } from 'react'
import { Container, Content, CustomForm } from './styles'
import { Form, Input, Button } from 'antd'
import { withRouter } from 'react-router-dom'

class RegistrationForm extends Component {
	constructor(props){
		super(props)
		this.state = {
			confirmDirty: false
		}
	}

	handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
      	const CryptoJS = require("crypto-js")
      	const cipherpass = CryptoJS.AES.encrypt(values.password, 'key');
      	let databody = {
		      firstName: values.firstName,
		      lastName: values.lastName,
		      email: values.email,
		      password: cipherpass.toString()
		    }
		    fetch(`${process.env.REACT_APP_HOST}/users/create`, {
		      method: 'POST',
		      headers: {'Content-Type':'application/json'},
		      body: JSON.stringify(databody)
		    })
		    .then(res => {
		      res.json().then(result => {
		      	console.log('test')
		        this.props.changeView('login')
		      })
		    })
      }
    })
  }

  handleConfirmBlur = (e) => {
    const value = e.target.value
    this.setState({ confirmDirty: this.state.confirmDirty || !!value })
  }

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form
    if (value && value !== form.getFieldValue('password')) {
      callback('Las contraseñas ingresadas no coinciden!');
    } else {
      callback();
    }
  }

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['passwordCheck'], { force: true });
    }
    callback();
  }

	render() {
		const { getFieldDecorator } = this.props.form;

		return (
			<Container>
				<Content>
					<CustomForm onSubmit={this.handleSubmit} className="login-form">
						<div className='title'>Ingresa tus datos y comencémos...</div>
		        <Form.Item>
		          {getFieldDecorator('firstName', {
		            rules: [{ required: true, message: 'Debe ingresar un nombre' }],
		          })(
		            <Input placeholder="Nombre" />
		          )}
		        </Form.Item>
		        <Form.Item>
		          {getFieldDecorator('lastName', {
		            rules: [{ required: true, message: 'Debe ingresar un apellido' }],
		          })(
		            <Input placeholder="Apellido" />
		          )}
		        </Form.Item>
		        <Form.Item>
		          {getFieldDecorator('email', {
		            rules: [{ required: true, message: 'Debe ingresar un email' }, { type: 'email', message: 'Ingresa un email válido' }],
		          })(
		            <Input placeholder="Email" />
		          )}
		        </Form.Item>
		        <Form.Item>
		          {getFieldDecorator('password', {
		            rules: [{ required: true, message: 'Debe ingresar una contraseña' }, { validator: this.validateToNextPassword }],
		          })(
		            <Input type='password' placeholder="Contraseña" />
		          )}
		        </Form.Item>
		        <Form.Item>
		          {getFieldDecorator('passwordCheck', {
		            rules: [{ required: true, message: 'Debe reingresar contraseña' },{ validator: this.compareToFirstPassword }],
		          })(
		            <Input type='password' onBlur={this.handleConfirmBlur} placeholder="Confirmar contraseña" />
		          )}
		        </Form.Item>
		        <Button onClick={() => this.props.changeView('login')} className="login-form-button">
	            Cancelar
	          </Button>
	          <Button type="primary" htmlType="submit" className="login-form-button">
	            Crear usuario
	          </Button>
		      </CustomForm>
				</Content>
			</Container>
		)
	}
}

const Registration = Form.create()(RegistrationForm)

export default withRouter(Registration)
