import React, { Component } from 'react'
import { Form, Icon, Input, Button, message } from 'antd'
import { Container, CustomForm } from './styles'
import { withRouter } from 'react-router-dom'

class LoginForm extends Component {

	handleSubmit = (e) => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
      	const CryptoJS = require("crypto-js")
      	const cipherpass = CryptoJS.AES.encrypt(values.password.toString(), 'key')
      	let databody = {
		      email: values.email,
		      password: cipherpass.toString()
		    }
		    fetch(`${process.env.REACT_APP_HOST}/users/validate`, {
		      method: 'POST',
		      headers: {
		      	'Accept': 'application/json',
		      	'Content-Type':'application/json'
		      },
		      body: JSON.stringify(databody)
		    })
		    .then(res => {
		      res.json().then(result => {
		        if (result.success) {
		        	sessionStorage.setItem('userId', result.user._id)
		        	this.props.history.push(`/admin`)
		        } else {
		        	message.error('Los datos ingresados son incorrectos')
		        }
		      })
		    })
      }
    });
  }

	render() {
		const { getFieldDecorator } = this.props.form

		return (
			<Container>
				<div className='welcome'>
					<h1>¿Listo para una barbacoa de ensueño?</h1>
					<ul>
						<li>Crea y administra tus propias barbacoas</li>
						<li>Busca barbacoas cerca tuyo y disfruta</li>
					</ul>
				</div>
				<CustomForm onSubmit={this.handleSubmit}>
	        <Form.Item>
	          {getFieldDecorator('email', {
	            rules: [{ required: true, message: 'Por favor ingresa un email' }, {type: 'email', message: 'Ingresa un email válido'}],
	          })(
	            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />
	          )}
	        </Form.Item>
	        <Form.Item>
	          {getFieldDecorator('password', {
	            rules: [{ required: true, message: 'Ingresa una contraseña!' }],
	          })(
	            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Contraseña" />
	          )}
	        </Form.Item>
	        <div className='actions'>
	        	<Button type="primary" htmlType="submit" className="login-form-button">
	            Ingresa
	          </Button>
	          o
	          <Button type='link' onClick={() => this.props.changeView('register')}>¡Regístrate!</Button>
	        </div>
	      </CustomForm>
			</Container>
		)
	}
}

const Login = Form.create()(LoginForm)

export default withRouter(Login)
