import styled from 'styled-components'
import { Form } from 'antd'

const Container = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  .welcome{
    font-size: 16px;
    color: white;
    margin-right: 20px;
    h1{
      color: white;
    }
  }
`
const CustomForm = styled(Form)`
  width: 300px;
  height: 200px;
  padding: 20px !important;
  border-radius: 6px;
  background-color: rgba(39, 35, 31, 0.7);
  .login-form-button{
    margin-right: 10px;
  }
  .actions{
    display: flex;
    align-items: center;
  }
`

export { Container, CustomForm }