import React, { Component } from 'react'
import { Upload, Button, Input, Form, Icon, message } from 'antd'
import { Container, CustomForm } from './styles'
import { withRouter } from 'react-router-dom'

const { TextArea } = Input

class BarbecueForm extends Component {
	constructor(props){
		super(props)
		this.state = {
		    fileList: [],
		    saving: false
		}
	}

	handleSubmit = (e) => {
    e.preventDefault()
    this.setState({ saving: true })
    this.props.form.validateFields((err, values) => {
      if (!err) {
      	if(values.image.file.status === 'done'){
					let formData = new FormData()
					formData.append('image', values.image.file.originFileObj)
					formData.append('model', values.model)
					formData.append('description', values.description)
					formData.append('zipcode', values.zipcode)
					formData.append('userId', sessionStorage.getItem('userId'))
			    fetch(`${process.env.REACT_APP_HOST}/barbecues/create`, {
			      method: 'POST',
			      body: formData
			    })
			    .then(response => response.json())
			    .then(result => {
			    	if(result.success){
			    		this.props.updateData(result.barbecue)
				    	this.setState({ saving: false })
				    	this.onRemove()
				    	this.props.form.resetFields()
			    	} else {
			    		message.error('No se ha podido encontrar el zipcode ingresado.')
			    	}
			    })
				}
      }
    });
  }

  handleCancel = () => {
		this.setState({ previewVisible: false })
	}

	handleChange = (e) => {
		this.setState({ fileList: e.fileList })
	}

	onRemove = () => {
		this.setState({ fileList: [] })
	}

	render() {
		const { getFieldDecorator } = this.props.form
		const { fileList, saving } = this.state

		const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">
        	<span>Subir imagen</span>
        	<span className='tip'>Recomendado 300x250 px</span>
        </div>
      </div>
    )

		return (
			<Container>
				<CustomForm onSubmit={this.handleSubmit}>
					<Form.Item>
	          {getFieldDecorator('image', {
	            rules: [{ required: true }],
	          })(
	            <Upload
			          action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
			          listType="picture-card"
			          fileList={fileList}
			          onChange={this.handleChange}
			          onRemove={this.onRemove}
			        >
			          {fileList.length >= 1 ? null : uploadButton}
		        </Upload>
	          )}
	        </Form.Item>
	        <Form.Item>
	          {getFieldDecorator('model', {
	            rules: [{ required: true }],
	          })(
	            <Input maxLength={20} placeholder="Modelo" />
	          )}
	        </Form.Item>
	        <Form.Item>
	          {getFieldDecorator('zipcode', {
	            rules: [{ required: true }],
	          })(
	            <Input placeholder="Zipcode" />
	          )}
	        </Form.Item>
	        <Form.Item>
	          {getFieldDecorator('description', {
	            rules: [{ required: true }],
	          })(
	            <TextArea autosize={{ minRows: 3, maxRows: 3 }} placeholder="Descripción" />
	          )}
	        </Form.Item>
	        <div className='actions'>
	        	<Button loading={saving} type="primary" htmlType="submit" className="login-form-button">
	            Guardar
	          </Button>
	        </div>
	      </CustomForm>
			</Container>
		)
	}
}

const Barbecue = Form.create()(BarbecueForm)

export default withRouter(Barbecue)
