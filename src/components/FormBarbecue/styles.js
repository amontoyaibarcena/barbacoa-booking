import styled from 'styled-components'
import { Form } from 'antd'

const Container = styled.div`
  height: 450px;
  width: 300px;
  display: flex;
  justify-content: center;
  padding: 15px;
  background-color: rgba(39, 35, 31, 0.2);
  border-radius: 4px;
  .welcome{
    font-size: 16px;
    color: white;
    margin-right: 20px;
    h1{
      color: white;
    }
  }
`
const CustomForm = styled(Form)`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-left: 15px;
  width: 100%;
  .ant-upload-list-picture-card .ant-upload-list-item,
  .ant-upload.ant-upload-select-picture-card{
    width: 200px;
    height: 200px;
    margin-right: 0px;
    margin-bottom: 10px;
  }
  .anticon-eye-o{
    display: none;
  }
  .ant-form-explain{
    display: none;
  }
  .ant-form-item{
    margin-bottom: 0px;
  }
  .ant-input{
    margin-bottom: 10px;
    width: 250px;
  }
  .ant-upload-text{
    display: flex;
    flex-direction: column;
    .tip{
      font-size:10px;
      color: rgba(0, 0, 0, 0.45);
    }
  }
`

export { Container, CustomForm }